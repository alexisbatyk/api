const express = require("express")
const app = express.Router()
const User = require("../models/user")

app.get("/", (req, res) => {
  res.end("ok!")
})

app.get("/task", async (req, res) => {
  console.log("req.user", req.user)

  const usr = await User.findOne({ name: req.user.user })

  if (!usr) {
    res.send({ error: "user not found" })
    return res.end()
  }

  const { taskList } = usr
  console.log("get list", usr)
  res.send(taskList || [])
  res.end()
})

app.get("/task/:id", async (req, res) => {
  const id = req.params.id
  console.log(`get one task id ${id}`)
  const usr = await User.findOne({
    name: req.user.user,
  }).lean()

  if (!usr) {
    res.send({ error: "user not found" })
    return res.end()
  }

  const { taskList } = usr

  const task = taskList.find(t => t._id == id)

  if (!task) {
    res.send({ error: "task not found" })
    return res.end()
  }

  res.send(task)
  res.end()
})

app.post("/task", async (req, res) => {
  const { title, description } = req.body
  console.log("create task")
  const usr = await User.findOneAndUpdate(
    { name: req.user.user },
    {
      $push: {
        taskList: { title, description },
      },
    },
    { new: true }
  ).lean()

  if (!usr) {
    res.send({ error: "user not found" })
    return res.end()
  }

  res.send(usr)
  res.end()
})

app.patch("/task/:id", async (req, res) => {
  const id = req.params.id
  const { title } = req.body
  console.log(`get one task id ${id}`)
  const usr = await User.findOne({
    name: req.user.user,
  })

  if (!usr) {
    res.send({ error: "user not found" })
    return res.end()
  }

  console.log(usr)

  const task = usr.taskList.find(t => t._id == id)

  if (task) {
    task.title = title
    usr.save()

    console.log("patch one task")
    res.send(usr)
  } else {
    res.send({ error: "task not found" })
  }
  res.end()
})

app.delete("/task/all", async (req, res) => {
  console.log(`delete all tasks`)
  const usr = await User.findOne({
    name: req.user.user,
  })
  if (!usr) {
    res.send({ error: "user not found" })
    return res.end()
  }

  const tasks = []

  usr.taskList = tasks
  usr.save()
  res.send(usr)
  res.end()
})

app.delete("/task/:id", async (req, res) => {
  const id = req.params.id

  console.log(`delete one task id ${id}`)
  const usr = await User.findOne({
    name: req.user.user,
    "taskList._id": id,
  })
  if (!usr) {
    res.send({ error: "user not found" })
    return res.end()
  }

  const tasks = usr.taskList.filter(t => t._id != id)

  if (tasks.length < usr.taskList.length) {
    usr.taskList = tasks
    usr.save()

    res.send(usr)
  } else {
    res.send({ error: "task not found" })
  }
  res.end()
})

module.exports = app

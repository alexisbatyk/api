const express = require("express")
const app = express.Router()
const jwt = require("jsonwebtoken")
const config = require("../config/config")
const User = require("../models/user")

const validateCredentials = (user = "") => {
  const acceptedUsers = [
    "alexis",
    "alexis-test",
    "joshua",
    "nathan",
    "fernando",
  ]

  if (acceptedUsers.includes(user.toLowerCase())) {
    return true
  } else {
    return false
  }
}

app.post("/", async (req, res) => {
  // console.log(req.body)
  const { user } = req.body
  if (validateCredentials(user)) {
    // does the user exists in the db?
    const dbUser = await User.findOneAndUpdate(
      { name: user },
      { lastLogin: new Date() },
      { upsert: true, new: true }
    )

    console.log(dbUser)

    const token = jwt.sign(
      {
        user: req.body.user,
        geo: "lat:12.13,long:42.32",
        createdAt: new Date().toUTCString(),
      },
      config.JWT_SECRET,
      { expiresIn: config.JWT_EXPIRES_IN }
    )
    res.send({ token })
    res.end()
  } else {
    res.sendStatus(401) // unauthorized
  }
})

module.exports = app

const mongoose = require("mongoose")
const config = require("../config/config")

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
}

mongoose.connect(process.env.DEV ? config.LOCAL_DB_URL : config.DB_URL, options)

const db = mongoose.connection

let DB_CONNECTED = false

module.exports = {
  db: db,
  DB_CONNECTED: DB_CONNECTED,
}

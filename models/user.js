const mongoose = require("mongoose")
const { TaskSchema } = require("./task")

const schema = new mongoose.Schema({
  name: { type: String, required: true },
  taskList: [TaskSchema],
  lastLogin: { type: Date, default: Date.now() },
})

const User = mongoose.model("User", schema)

module.exports = User

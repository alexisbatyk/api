const request = require("supertest")
const should = require("should")
const app = require("../server/app")

describe("POST /api/token", () => {
  it("create token", done => {
    request(app)
      .post("/api/token")
      .send({ user: "alexis" }) // it should be user and password
      .expect("Content-Type", /json/)
      .expect(200)
      .end((err, res) => {
        should.exists(res.body.token)
        done()
      })
  })

  it("create token with a non registered user", done => {
    request(app)
      .post("/api/token")
      .send({ user: "wrong-user" }) // it should be user and password
      .expect("Content-Type", /text/)
      .expect(401, done) // 401 Unauthorized
  })
})

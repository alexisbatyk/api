const request = require("supertest")
const app = require("../server/app")

describe("GET /", () => {
  it("server is up, responds with ok!", done => {
    request(app)
      .get("/api/")
      .expect("Content-Length", "3")
      .expect(200)
      .expect("ok!", done)
  })
})

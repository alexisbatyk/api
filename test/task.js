const request = require("supertest")
const should = require("should")
const app = require("../server/app")
const { EMPTY_ARRAY } = require("../utils/constants")
// there should be another env for testing with its own db

describe("GET /api/task", () => {
  it("get empty task list", done => {
    request(app)
      .post("/api/token")
      .send({ user: "alexis-test" })
      .expect("Content-Type", /json/)
      .expect(200)
      .end((err, res) => {
        const {
          body: { token },
        } = res
        should.exists(token)

        request(app)
          .get("/api/task")
          .set("Authorization", `Bearer ${token}`)
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) => {
            const { body } = res
            should.deepEqual(body, EMPTY_ARRAY)
            done()
          })
      })
  })
})

describe("POST /api/task", () => {
  it("create a task", done => {
    done()
  })
})

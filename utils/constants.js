const EMPTY_ARRAY = []
const EMPTY_OBJECT = {}

module.exports = {
  EMPTY_ARRAY,
  EMPTY_OBJECT,
}

const dbuser = "dbuser" // this should be in
const dbpass = "dbpass" // envs

module.exports = {
  SERVER_PORT: 4000, // it should be get from process.env.PORT or something like that
  LOCAL_DB_URL: "mongodb://localhost/TaskList", // local db
  DB_URL: `mongodb+srv://${dbuser}:${dbpass}@cluster0.r0v0n.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`, // mongodb atlas
  JWT_SECRET: "non-secure-secret", // this shouldnt be here,
  JWT_EXPIRES_IN: "24h",
}

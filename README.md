# Task List API

### Specs:

- Express REST with JWT Auth
- MongoDB with mongoose
- Testing API with supertest and mocha

## Start server

Dependencies:

- Node
- Docker

### Install

`npm install`

### Dev server with local db

Start the local docker db

`npm run mongodb`

Stop and kill mongodb instance

`npm run stop-mongodb && npm run rm-mongodb`

Start the dev server

`npm run dev`

### Start prod server

`npm start`

or

`node server/index.js`

## Testing

`npm test`

## Linting (ESLint)

`npm run lint`

## Front end deployment

`git add public/ && git commit -m "frontend deployment" && git push`

This happens when deploying with heroku, with another provider this wouldn't happen. The frontend project would be able to deploy itself in its own pipeline.

## Tech debt

- More testing
- Auth with user and password
- NLP to predict words when adding tasks
- Move to .env file all config

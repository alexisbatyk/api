const app = require("./app")
const config = require("../config/config")

app.listen(process.env.PORT || config.SERVER_PORT, () => {
  console.log(`server is up at ${process.env.PORT || config.SERVER_PORT}`)
})

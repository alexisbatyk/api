const express = require("express")
const app = express()
var jwt = require("express-jwt")
const config = require("../config/config")
const { db } = require("../db/database")
const routes = require("../routes/routes")
const authRouter = require("../routes/auth")
const path = require("path")

db.on("error", err => {
  console.error("couldnt connect to db")
  console.log("couldnt connect to db", err)
  // handle db errors
})

db.on("connected", () => {
  console.log("db connected")
})

app.use(express.json())

// this is only for dev porpouse
// the api should be served in APP_URL/api/
// and the react app in APP_URL/
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  )
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, PATCH")
  next()
})

app.use("/api/task", jwt({ secret: config.JWT_SECRET, algorithms: ["HS256"] }))

app.use("/api/token", authRouter)

app.use("/api/", routes)

const publicPath = path.join(__dirname, "..", "public")

app.use(express.static(publicPath))

module.exports = app
